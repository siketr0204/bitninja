import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../domain/post";
import {LogService} from "../services/log.service";
import {Subscription} from "rxjs";
import {MatDialogRef} from "@angular/material/dialog";
import {SnackbarService} from "../services/snackbar.service";

@Component({
  selector: 'app-add-post-dialog',
  templateUrl: './add-post-dialog.component.html',
  styleUrls: ['./add-post-dialog.component.css']
})
export class AddPostDialogComponent implements OnInit {
  private static readonly URL_POSTS = `https://jsonplaceholder.typicode.com/posts`;
  private subscription: Subscription;
  body: string;
  title: string;
  saving: boolean;

  constructor(private _httpClient: HttpClient,
              private _logService: LogService,
              private _dialogRef: MatDialogRef<AddPostDialogComponent>,
              private _snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
  }

  onAddClick(): void {
    this.saving = true;
    let newPost: Post = {
      body: this.body,
      title: this.title,
      userId: 1
    };
    this.subscription = this._httpClient.post<Post>(AddPostDialogComponent.URL_POSTS, newPost)
    .subscribe((post) => this.onAddPostSuccess(post), error => this.onHttpError(error));
  }

  private onAddPostSuccess(post: Post) {
    this.saving = false;
    this.subscription.unsubscribe();
    this._dialogRef.close(post);
  }

  private onHttpError(error: any) {
    this.saving = false;
    this.subscription.unsubscribe();
    this._logService.error(error);
    this._snackbarService.error(error.toString());
  }

  isAddDisabled() {
    return this.title === null || this.title === undefined || this.body === null || this.body === undefined;
  }
}
