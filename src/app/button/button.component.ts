import {Component, Input, OnInit} from '@angular/core';
import {isNotNullOrUndefined} from "codelyzer/util/isNotNullOrUndefined";

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  @Input()
  public buttonSize: ButtonSizes = ButtonSizes.NORMAL;
  @Input()
  public buttonColor: ButtonColors = ButtonColors.BLUE;
  @Input()
  public text: string;
  @Input()
  public icon: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  getHtmlClasses(): string {
    const classes = [];

    this.addButtonSizeClass(classes);
    this.addButtonColorClass(classes);
    this.addColorWhiteClass(classes);
    this.addIconClass(classes);

    return classes.join(' ');
  }

  private addColorWhiteClass(classes: any[]) {
    classes.push('color-white');
  }

  private addIconClass(classes: any[]) {
    if (isNotNullOrUndefined(this.icon)) {
      classes.push(this.icon);
      classes.push('icon-button');
    }
  }

  private addButtonColorClass(classes: any[]) {
    switch (this.buttonColor) {
      case ButtonColors.DARK_GREY:
        classes.push('bg-color-dark-grey');
        break;
      case ButtonColors.VIOLET:
        classes.push('bg-color-violet');
        break;
      case ButtonColors.RED:
        classes.push('bg-color-red');
        break;
      case ButtonColors.BLUE:
        classes.push('bg-color-blue');
        break;
      case ButtonColors.DARK_RED:
        classes.push('bg-color-dark-red');
        break;
    }
  }

  private addButtonSizeClass(classes: any[]) {
    switch (this.buttonSize) {
      case ButtonSizes.NORMAL:
        classes.push('normal');
        break;
      case ButtonSizes.MEDIUM:
        classes.push('medium');
        break;
      case ButtonSizes.LARGE:
        classes.push('large');
        break;
    }
  }
}

export enum ButtonSizes {
  NORMAL,
  MEDIUM,
  LARGE
}

export enum ButtonColors {
  DARK_GREY,
  VIOLET,
  RED,
  BLUE,
  DARK_RED
}


