import {Comment} from "./comment";

export interface Post {
  loading?: boolean;
  opened?: boolean;
  loaded?: Boolean;
  userId?: number,
  id?: number,
  title: string,
  body: string,
  comments?: Comment[];
}
