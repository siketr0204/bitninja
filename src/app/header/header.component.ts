import {Component, OnInit} from '@angular/core';
import {ButtonColors, ButtonSizes} from "../button/button.component";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  buttonSizes = ButtonSizes;
  buttonColors = ButtonColors;
  private static readonly GITLAB_URL = 'https://gitlab.com/siketr0204/bitninja';
  private static readonly WINDOW_BLANK = '_blank';

  constructor() {
  }

  ngOnInit(): void {
  }

  onClickGitlab() {
    window.open(HeaderComponent.GITLAB_URL, HeaderComponent.WINDOW_BLANK);
  }
}
