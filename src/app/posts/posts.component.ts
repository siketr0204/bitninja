import {Component, OnInit} from '@angular/core';
import {Post} from "../domain/post";
import {HttpClient} from "@angular/common/http";
import {LogService} from "../services/log.service";
import {ButtonColors, ButtonSizes} from "../button/button.component";
import {Comment} from "../domain/comment";
import {Subscription} from "rxjs";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AddPostDialogComponent} from "../add-post-dialog/add-post-dialog.component";
import {SnackbarService} from "../services/snackbar.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  buttonSizes = ButtonSizes;
  buttonColors = ButtonColors;
  posts: Post[] = [];
  subscriptions: Subscription[] = [];

  constructor(private _httpClient: HttpClient, private _logService: LogService, private _dialog: MatDialog, private _snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.loadPosts();
  }

  private loadPosts() {
    this._httpClient.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
    .subscribe(posts => this.onLoadPostSuccess(posts), error => this.onHttpError(error));
  }

  private onHttpError(error: Error) {
    this._logService.error(error);
    this._snackbarService.error(error.toString());
  }

  private onLoadPostSuccess(posts: Post[]) {
    this.posts = posts;
  }

  onClickShowComments(post: Post) {
    if (post.loading) {
      return;
    }

    if (post.loaded) {
      post.opened = !post.opened;
    } else {
      this.loadComments(post);
    }
  }

  private loadComments(post: Post) {
    post.loading = true;
    let subscription: Subscription = this._httpClient.get<Comment[]>(`https://jsonplaceholder.typicode.com/posts/${post.id}/comments`)
    .subscribe(comments => this.onLoadCommentsSuccess(post, comments), error => this.onHttpError(error));
    this.subscriptions.push(subscription);
  }

  private onLoadCommentsSuccess(post: Post, comments: Comment[]) {
    post.comments = comments;
    post.loaded = true;
    post.opened = true;
    post.loading = false;
  }

  onClickAddPost() {
    const dialogRef: MatDialogRef<AddPostDialogComponent> = this._dialog.open(AddPostDialogComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().toPromise().then(result => {
      if (result) {
        this._snackbarService.info('Új bejegyzés sikeresen hozzáadva');
      }
    });
  }


}
