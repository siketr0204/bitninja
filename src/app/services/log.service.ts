import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LogService {

  constructor() {
  }

  info(text: any) {
    const logFn: Function = (console).info || console.log;
    logFn.apply(console, [text]);
  }

  error(text: any) {
    const logFn: Function = (console).error || console.log;
    logFn.apply(console, [text]);
  }

  warn(text: any) {
    const logFn: Function = (console).warn || console.log;
    logFn.apply(console, [text]);
  }

  public separator(prefix: string = ''): void {
    this.info(prefix + '-------------------------------------------');
  }
}
