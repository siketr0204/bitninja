import {MatSnackBar} from '@angular/material/snack-bar';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  constructor(private snackBar: MatSnackBar) {
  }

  error(message: string) {
    this.snackBar.open(message, '', {duration: 5000, panelClass: 'error-panel'});
  }

  info(message: string, action?: string) {
    this.snackBar.open(message, action, {duration: 5000});
  }
}
